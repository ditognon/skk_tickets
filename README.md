# README

## Information
* Ruby 2.7.1
* Rails 6.0.4.4
* node v14.18.3
* yarn 1.22.17
* Devise >= 4.8.1 is used for user managment
* Database seed is in db/seeds.rb
