class AddNameToTicket < ActiveRecord::Migration[6.0]
  def change
    add_column :tickets, :name, :string
  end
end
