class AddEmailToTicket < ActiveRecord::Migration[6.0]
  def change
    add_column :tickets, :email, :string
  end
end
