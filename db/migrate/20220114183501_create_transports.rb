class CreateTransports < ActiveRecord::Migration[6.0]
  def change
    create_table :transports do |t|
      t.datetime :departure
      t.datetime :arrival
      t.integer :nTickets
      t.references :company, null: false, foreign_key: true


      t.timestamps
    end
  end
end
