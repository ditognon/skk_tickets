puts "Seeding..."

# Transport Company seeding
c1 = Company.create(name: 'Prijevoznik1')
c2 = Company.create(name: 'Prijevoznik2')
c3 = Company.create(name: 'Prijevoznik3')

# Transport seeding
# Company 1
# In half an hour, to test cancelation conditions
Transport.create(departure: Time.new() + 1800, arrival: Time.new() + 10000, nTickets: 50, price:45, company: c1)
# In one hour and 5 minutes, to test cancelation conditions
Transport.create(departure: Time.new() + 3900, arrival: Time.new() + 10000, nTickets: 50, price:45, company: c1)
Transport.create(departure: DateTime.strptime("01/25/2022 12:00", "%m/%d/%Y %H:%M"), arrival: DateTime.strptime("01/25/2022 18:00", "%m/%d/%Y %H:%M"), nTickets: 25, price:50, company: c1)
Transport.create(departure: DateTime.strptime("01/25/2022 23:45", "%m/%d/%Y %H:%M"), arrival: DateTime.strptime("01/26/2022 03:00", "%m/%d/%Y %H:%M"), nTickets: 25, price:70, company: c1)
Transport.create(departure: DateTime.strptime("01/17/2022 13:30", "%m/%d/%Y %H:%M"), arrival: DateTime.strptime("01/17/2022 15:00", "%m/%d/%Y %H:%M"), nTickets: 55, price:63, company: c1)

# Company 2
# Testing buying with 0 tickets
Transport.create(departure: DateTime.strptime("02/28/2022 9:00", "%m/%d/%Y %H:%M"), arrival: DateTime.strptime("02/28/2022 11:00", "%m/%d/%Y %H:%M"), nTickets: 0, price:52, company: c2)
Transport.create(departure: DateTime.strptime("12/25/2021 8:00", "%m/%d/%Y %H:%M"), arrival: DateTime.strptime("12/25/2021 10:00", "%m/%d/%Y %H:%M"), nTickets: 1, price:44, company: c2)
Transport.create(departure: DateTime.strptime("03/25/2022 9:00", "%m/%d/%Y %H:%M"), arrival: DateTime.strptime("03/25/2022 11:00", "%m/%d/%Y %H:%M"), nTickets: 3, price:110, company: c2)
Transport.create(departure: DateTime.strptime("01/25/2022 8:00", "%m/%d/%Y %H:%M"), arrival: DateTime.strptime("01/25/2022 10:00", "%m/%d/%Y %H:%M"), nTickets: 20, price:35, company: c2)

# Company 3
# Happens in a long time
Transport.create(departure: DateTime.strptime("01/25/2029 8:00", "%m/%d/%Y %H:%M"), arrival: DateTime.strptime("01/25/2029 10:00", "%m/%d/%Y %H:%M"), nTickets: 10, price:50, company: c3)
# Happened a few years ago
Transport.create(departure: DateTime.strptime("08/16/2017 22:50", "%m/%d/%Y %H:%M"), arrival: DateTime.strptime("08/17/2017 02:20", "%m/%d/%Y %H:%M"), nTickets: 10, price:50, company: c3)
Transport.create(departure: DateTime.strptime("01/28/2022 16:00", "%m/%d/%Y %H:%M"), arrival: DateTime.strptime("01/28/2022 17:00", "%m/%d/%Y %H:%M"), nTickets: 10, price:55, company: c3)
Transport.create(departure: DateTime.strptime("01/19/2022 14:00", "%m/%d/%Y %H:%M"), arrival: DateTime.strptime("01/19/2022 16:10", "%m/%d/%Y %H:%M"), nTickets: 10, price:25, company: c3)


# Random transports to fill up the database
Transport.create(departure: DateTime.strptime("02/07/2022 12:25", "%m/%d/%Y %H:%M"), arrival: DateTime.strptime("02/07/2022 13:00", "%m/%d/%Y %H:%M"), nTickets: 40, price:52, company: c2)
Transport.create(departure: DateTime.strptime("12/25/2022 13:11", "%m/%d/%Y %H:%M"), arrival: DateTime.strptime("12/25/2022 14:11", "%m/%d/%Y %H:%M"), nTickets: 10, price:44, company: c3)
Transport.create(departure: DateTime.strptime("03/13/2022 17:45", "%m/%d/%Y %H:%M"), arrival: DateTime.strptime("03/13/2022 18:34", "%m/%d/%Y %H:%M"), nTickets: 3, price:110, company: c2)
Transport.create(departure: DateTime.strptime("05/30/2022 01:10", "%m/%d/%Y %H:%M"), arrival: DateTime.strptime("05/30/2022 02:00", "%m/%d/%Y %H:%M"), nTickets: 2, price:35, company: c2)
Transport.create(departure: DateTime.strptime("01/01/2023 04:00", "%m/%d/%Y %H:%M"), arrival: DateTime.strptime("01/01/2023 06:10", "%m/%d/%Y %H:%M"), nTickets: 15, price:34, company: c1)
Transport.create(departure: DateTime.strptime("12/08/2022 23:00", "%m/%d/%Y %H:%M"), arrival: DateTime.strptime("12/09/2022 01:00", "%m/%d/%Y %H:%M"), nTickets: 20, price:51, company: c2)
Transport.create(departure: DateTime.strptime("09/24/2022 12:55", "%m/%d/%Y %H:%M"), arrival: DateTime.strptime("09/24/2022 15:00", "%m/%d/%Y %H:%M"), nTickets: 30, price:96, company: c1)
Transport.create(departure: DateTime.strptime("08/31/2022 19:00", "%m/%d/%Y %H:%M"), arrival: DateTime.strptime("08/31/2022 20:30", "%m/%d/%Y %H:%M"), nTickets: 65, price:28, company: c1)
Transport.create(departure: DateTime.strptime("09/02/2022 15:53", "%m/%d/%Y %H:%M"), arrival: DateTime.strptime("09/02/2022 17:00", "%m/%d/%Y %H:%M"), nTickets: 11, price:51, company: c1)
Transport.create(departure: DateTime.strptime("10/06/2022 15:09", "%m/%d/%Y %H:%M"), arrival: DateTime.strptime("10/06/2022 17:00", "%m/%d/%Y %H:%M"), nTickets: 25, price:31, company: c2)
Transport.create(departure: DateTime.strptime("11/05/2022 13:00", "%m/%d/%Y %H:%M"), arrival: DateTime.strptime("11/06/2022 14:40", "%m/%d/%Y %H:%M"), nTickets: 20, price:11, company: c3)
Transport.create(departure: DateTime.strptime("11/11/2022 10:20", "%m/%d/%Y %H:%M"), arrival: DateTime.strptime("11/11/2022 10:55", "%m/%d/%Y %H:%M"), nTickets: 10, price:41, company: c3)

puts "Seeding done."