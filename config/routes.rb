Rails.application.routes.draw do
  devise_for :users

  root "home#index"

  get "/buy/:id", to: "home#checkout", as: "checkout"
  post "/buy/:id", to: "home#buy", as: "buy"

  delete "/cancel_ticket/:id", to: "home#cancel_user_ticket", as: "cancel_ticket"

  get "/user_tickets", to: "home#user_tickets"

end
