class User < ApplicationRecord

  has_many :tickets, dependent: :destroy
  has_many :transports, through: :tickets

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
end
