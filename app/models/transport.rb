class Transport < ApplicationRecord
  belongs_to :company

  has_many :tickets, dependent: :destroy
  has_many :users, through: :tickets
end
