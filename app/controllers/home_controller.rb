class HomeController < ApplicationController
    before_action :authenticate_user!, except: [:index]

    # Shows all available transports - doesn't show past transports
    def index
      @transports = []
      tmp_transport = Transport.order("departure ASC")
      current_time = Time.new()
      tmp_transport.each do |tmp_transport|
        if tmp_transport.departure > current_time
          @transports.append(tmp_transport)
        end
      end
    end

    # Shows all tickets purchased by a user - only future ones
    def user_tickets
      @tickets = []
      tmp_tickets = current_user.tickets
      current_time = Time.new()
      tmp_tickets.each do |tmp_ticket|
        if tmp_ticket.transport.departure > current_time
          @tickets.append(tmp_ticket)
        end
      end
    end

    # Cancels a ticket if there is more than an hour until departure
    # Deletes ticket from database
    def cancel_user_ticket
        @ticket = Ticket.find(params[:id])
        if (@ticket.transport.departure - Time.new()) > 3600
          @ticket.destroy
          redirect_to user_tickets_path, notice: "Otkazana karta!"
        else
          redirect_to user_tickets_path, notice: "Nije moguce otkazati kartu sat vremena prije poslaska!"
        end
        
    end

    # Buy menu
    def checkout
        @transport = Transport.find(params[:id])
    end

    # Checks all conditions and inserts ticket into database
    def buy
        @transport = Transport.find(params[:id])
        @ticket = Ticket.new(user: current_user, transport: @transport, name: params[:input_name] , email: params[:input_email])

        # Check transport is in the future
        if @transport.departure > Time.new()
          # Check available tickets
          if @transport.nTickets - @transport.tickets.length > 0
            # Check card number
            if params[:input_card_num].length > 10
              # Check name
              if params[:input_name].length > 2
                # Check email
                if params[:input_email].length > 5

                  if @ticket.save # Save ticket to database
                    redirect_to user_tickets_path, notice: "Uspjesno kupljena karta."
                  else
                      redirect_to root_path, notice: "Transakcija neuspjela! Karta nije kupljena."
                  end

                else
                  redirect_to root_path, notice: "Morate unijeti email!"
                end
              else
                redirect_to root_path, notice: "Morate unijeti ime!"
              end
            else
              redirect_to root_path, notice: "Morate unijeti broj kartice!"
            end
          else
            redirect_to root_path, notice: "Sve karte su rasprodane! Karta nije kupljena."
          end
        else
          redirect_to root_path, notice: "Nije moguce kupiti karte u proslosti!"
        end
      end
  end